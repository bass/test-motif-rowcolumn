/*
 * $Id$
 */
#include <stdlib.h>
#include <wchar.h>

#include <locale.h>
#include <langinfo.h>

#include <Xm/XmAll.h>

#undef USE_RENDER_TABLE

const char *keyBackground = "#dcdad5";

const char *valueBackground = "#ffffff";

void addLine(Widget parent,
		char *keyWidgetName,
		char *keyLabel,
		char *valueWidgetName,
		const XmString valueLabel);

Pixel toColor(const Widget widget, const char *name) {
	XrmValue from;
	from.addr = (char *) name;
	from.size = strlen(name) + 1;

	XrmValue to;
	to.addr = NULL;

	XtConvertAndStore(widget, XmRString, &from, XmRPixel, &to);

	if (to.addr != NULL) {
		return *((Pixel *) to.addr);
	}

	return XmUNSPECIFIED_PIXEL;
}

int main(int argc, char *argv[]) {
	const char *locale = setlocale(LC_ALL, "");
	if (locale == NULL) {
		fprintf(stderr, "Locale unavailable.\n");
	}

	char *localeCharset = nl_langinfo(CODESET);
	printf("locale: %s\n", locale);
	printf("run-time charset: %s\n", localeCharset);

	XtAppContext appContext;

	XtSetLanguageProc(NULL, (XtLanguageProc) NULL, (XtPointer) NULL);

	const char *applicationClass = "XmRowColumnTest";
	String fallbackResources[] = {
			// TODO: use XmNnoFontCallback (see http://www.ist.co.uk/motif/books/vol6A/ch-24.fm.html)
			"*.XmLabel.fontList: 		-microsoft-verdana-medium-r-normal--*-90-*-*-p-*-*-*, \
							-microsoft-tahoma-medium-r-normal--*-90-*-*-p-*-*-*, \
							-monotype-arial-medium-r-normal--*-90-*-*-p-*-*-*, \
			                                -*-*-medium-r-normal--*-90-*-*-p-*-*-*:",
#ifdef USE_RENDER_TABLE
			"*singleByteRendition.fontName: -monotype-arial-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251, "
							"-microsoft-verdana-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251, "
							"-microsoft-tahoma-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251, "
							"-*-*-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251:",
			"*singleByteRendition.fontType: FONT_IS_FONTSET",
			"*singleByteRendition.renditionForeground: red",
			"*singleByteRendition.renditionBackground: blue",
			"*singleByteRendition.strikethruType: AS_IS",
			"*singleByteRendition.underlineType: SINGLE_LINE",
			"*.labelSingleByte.renderTable:	singleByteRendition",
			"*.labelUnicode.renderTable: ",
			"*.labelUnicode.renderTable.fontName:	-monotype-arial-medium-r-normal--*-90-*-*-p-*-iso10646-1",
			"*.labelUnicode.renderTable.fontType: FONT_IS_FONT",
#else
			"*.labelSingleByte.fontList:	-monotype-arial-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251, "
							"-microsoft-verdana-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251, "
							"-microsoft-tahoma-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251, "
							"-misc-fixed-medium-r-normal--*-90-*-*-c-*-microsoft-cp1251, ",
							"-monotype-arial-regular-r-normal--*-90-*-*-p-*-ansi-1251, " // Xsun
							"-dt-application-medium-r-normal-sans-*-90-*-*-p-*-ansi-1251, " // Xsun
							"-*-*-medium-r-normal--*-90-*-*-p-*-microsoft-cp1251:",
			"*.labelSingleByte.foreground:	red",
			"*.labelUnicode.fontList:	-monotype-arial-medium-r-normal--*-90-*-*-p-*-iso10646-1, \
							-microsoft-verdana-medium-r-normal--*-90-*-*-p-*-iso10646-1, \
							-microsoft-tahoma-medium-r-normal--*-90-*-*-p-*-iso10646-1, \
							-misc-fixed-medium-r-normal--*-90-*-*-c-*-iso10646-1, \
							-monotype-arial-regular-r-normal--*-90-*-*-p-*-ansi-1251, \
							-dt-application-medium-r-normal-sans-*-90-*-*-p-*-ansi-1251, \
							-*-*-medium-r-normal--*-90-*-*-p-*-iso10646-1:",
			"*.labelUnicode.foreground:	green",
			"*.labelLocale.fontList:	-monotype-arial-medium-r-normal--*-90-*-*-p-*-*-*",
			"*.labelLocale.foreground:	blue",
#endif // USE_RENDER_TABLE
			NULL,
	};
	const Widget topLevel = XtVaAppInitialize(&appContext, applicationClass, NULL, 0, &argc, argv, fallbackResources, NULL);
	XtVaSetValues(topLevel, XmNtitle, "XmRowColumn Test, " XmVERSION_STRING, NULL);
	XtVaSetValues(topLevel, XmNiconName, "XmRowColumn Test, " XmVERSION_STRING " (Icon)", NULL);

#if defined(__sun) && defined(__SVR4)
	const Widget mainWindow = XmCreateMainWindow(topLevel, "mainWindow", NULL, 0);
	XtManageChild(mainWindow);
#else
	const Widget mainWindow = XmVaCreateManagedMainWindow(topLevel, "mainWindow", NULL);
#endif

#if defined(__sun) && defined(__SVR4)
	const Widget rowColumn = XmCreateRowColumn(mainWindow, "rowColumn", NULL, 0);
	XtManageChild(rowColumn);
#else
	const Widget rowColumn = XmVaCreateManagedRowColumn(mainWindow, "rowColumn",
			XmNrowColumnType, XmWORK_AREA,
			NULL);
#endif

	XtVaSetValues(rowColumn, XmNpacking, XmPACK_COLUMN, NULL);
	XtVaSetValues(rowColumn, XmNorientation, XmHORIZONTAL, NULL);
	const char *rowColumnBackground = "#dcdad5";
//	XtVaSetValues(rowColumn, XtVaTypedArg, XmNbackground, XmRString, rowColumnBackground, strlen(rowColumnBackground) + 1, NULL);
	XtVaSetValues(rowColumn, XmNbackground, toColor(rowColumn, rowColumnBackground), NULL);

	const XmStringCharSet singleByteCharset = "CP1251";
	const XmStringCharSet unicodeCharset = "UTF-8";

	char *s = "Cyrillic: ��������";
	char *mbs = "Latin-1: \u00c0\u00c1\u00c2\u00c3\u00c4\u00c5\u00e0\u00e1\u00e2\u00e3\u00e4\u00e5; Cyrillic: \u0410\u0411\u0412\u0413\u0430\u0431\u0432\u0433";
	wchar_t *wcs = L"Latin-1: \u00c0\u00c1\u00c2\u00c3\u00c4\u00c5\u00e0\u00e1\u00e2\u00e3\u00e4\u00e5; Cyrillic: \u0410\u0411\u0412\u0413\u0430\u0431\u0432\u0433";

	char *singleByteLabelClass = "labelSingleByte";
	char *unicodeLabelClass = "labelUnicode";
	char *localeLabelClass = "labelLocale";

	int columnCount = 0;

	{
		const XmString labelString1 = XmStringCreateLocalized(s);
		addLine(rowColumn, "key1", "XmStringCreateLocalized():", singleByteLabelClass, labelString1);
		XmStringFree(labelString1);
		columnCount++;
	}

#if 0 // UTF-8 locale only
	{
		const XmString labelString1 = XmStringCreateLocalized(mbs);
		addLine(rowColumn, "key1", "XmStringCreateLocalized():", unicodeLabelClass, labelString1);
		XmStringFree(labelString1);
		columnCount++;
	}
#endif

	/*
	 * For single-byte strings, 2nd argument to XmStringCreate() should match the locale.
	 */
	{
		const XmString labelString2 = XmStringCreate(s, localeCharset);
		addLine(rowColumn, "key2", "XmStringCreate(single-byte string, locale charset):", singleByteLabelClass, labelString2);
		XmStringFree(labelString2);
		columnCount++;
	} {
		const XmString labelString2 = XmStringCreate(s, singleByteCharset);
		addLine(rowColumn, "key2", "XmStringCreate(single-byte string, \"CP1251\"):", singleByteLabelClass, labelString2);
		XmStringFree(labelString2);
		columnCount++;
	} {
		const XmString labelString2 = XmStringCreate(s, XmSTRING_DEFAULT_CHARSET);
		addLine(rowColumn, "key2", "XmStringCreate(single-byte string, XmSTRING_DEFAULT_CHARSET):", singleByteLabelClass, labelString2);
		XmStringFree(labelString2);
		columnCount++;
	} {
		const XmString labelString2 = XmStringCreate(s, "");
		addLine(rowColumn, "key2", "XmStringCreate(single-byte string, \"\"):", singleByteLabelClass, labelString2);
		XmStringFree(labelString2);
		columnCount++;
	}


	{
		const XmString labelString2 = XmStringCreate(mbs, unicodeCharset);
		addLine(rowColumn, "key2", "XmStringCreate(multi-byte string, \"UTF-8\"):", unicodeLabelClass, labelString2);
		XmStringFree(labelString2);
		columnCount++;
	}
#if 0 // UTF-8 locale only
	{
		const XmString labelString2 = XmStringCreate(mbs, localeCharset);
		addLine(rowColumn, "key2", "XmStringCreate(multi-byte string, locale charset):", unicodeLabelClass, labelString2);
		XmStringFree(labelString2);
		columnCount++;
	}
	{
		const XmString labelString2 = XmStringCreate(mbs, XmSTRING_DEFAULT_CHARSET);
		addLine(rowColumn, "key2", "XmStringCreate(multi-byte string, XmSTRING_DEFAULT_CHARSET):", unicodeLabelClass, labelString2);
		XmStringFree(labelString2);
		columnCount++;
	}
#endif


	/*
	 * For XmStringGenerate() in XmCHARSET_TEXT mode, tag can be either the
	 * name of the 8-bit charset, XmFONTLIST_DEFAULT_TAG, or NULL.
	 */
	{
		const XmString labelString6 = XmStringGenerate(s, XmFONTLIST_DEFAULT_TAG, XmCHARSET_TEXT, NULL);
		addLine(rowColumn, "key4", "XmStringGenerate(XmFONTLIST_DEFAULT_TAG, XmCHARSET_TEXT):", singleByteLabelClass, labelString6);
		XmStringFree(labelString6);
		columnCount++;
	}
	{
		const XmString labelString6 = XmStringGenerate(s, NULL, XmCHARSET_TEXT, NULL);
		addLine(rowColumn, "key4", "XmStringGenerate(NULL, XmCHARSET_TEXT):", singleByteLabelClass, labelString6);
		XmStringFree(labelString6);
		columnCount++;
	}
	{
		const XmString labelString6 = XmStringGenerate(s, singleByteCharset, XmCHARSET_TEXT, NULL);
		addLine(rowColumn, "key4", "XmStringGenerate(single-byte string, CP1251, XmCHARSET_TEXT):", singleByteLabelClass, labelString6);
		XmStringFree(labelString6);
		columnCount++;
	}
	{
		const XmString labelString6 = XmStringGenerate(mbs, unicodeCharset, XmCHARSET_TEXT, NULL);
		addLine(rowColumn, "key4", "XmStringGenerate(multi-byte string, XmCHARSET_TEXT):", unicodeLabelClass, labelString6);
		XmStringFree(labelString6);
		columnCount++;
	}

#if 0  // UTF-8 locale only
	{
		const XmString labelString6 = XmStringGenerate(mbs, XmFONTLIST_DEFAULT_TAG, XmCHARSET_TEXT, NULL);
		addLine(rowColumn, "key4", "XmStringGenerate(XmFONTLIST_DEFAULT_TAG, XmCHARSET_TEXT):", unicodeLabelClass, labelString6);
		XmStringFree(labelString6);
		columnCount++;
	} {
		const XmString labelString6 = XmStringGenerate(mbs, NULL, XmCHARSET_TEXT, NULL);
		addLine(rowColumn, "key4", "XmStringGenerate(NULL, XmCHARSET_TEXT):", unicodeLabelClass, labelString6);
		XmStringFree(labelString6);
		columnCount++;
	}
#endif




	XtVaSetValues(rowColumn, XmNnumColumns, columnCount, NULL); // For XmHORIZONTAL orientation, this is actually a number of rows

	XtRealizeWidget(topLevel);
	XtAppMainLoop(appContext);

	return 0;
}

void addLine(Widget parent,
		char *keyWidgetName,
		char *keyLabel,
		char *valueWidgetName,
		const XmString valueLabel) {
#if defined(__sun) && defined(__SVR4)
	const Widget label0 = XmCreateLabel(parent, keyWidgetName, NULL, 0);
	XtManageChild(label0);
#else
	const Widget label0 = XmVaCreateManagedLabel(parent, keyWidgetName, NULL);
#endif
	const XmString labelString0 = XmStringCreate(keyLabel, XmSTRING_DEFAULT_CHARSET);
	XtVaSetValues(label0, XmNlabelString, labelString0, NULL);
	XtVaSetValues(label0, XtVaTypedArg, XmNbackground, XmRString, keyBackground, strlen(keyBackground) + 1, NULL);
	XmStringFree(labelString0);


#if defined(__sun) && defined(__SVR4)
	const Widget label1 = XmCreateLabel(parent, valueWidgetName, NULL, 0);
	XtManageChild(label1);
#else
	const Widget label1 = XmVaCreateManagedLabel(parent, valueWidgetName, NULL);
#endif
	XtVaSetValues(label1, XmNlabelString, valueLabel, NULL);
	XtVaSetValues(label1, XtVaTypedArg, XmNbackground, XmRString, valueBackground, strlen(valueBackground) + 1, NULL);
}
